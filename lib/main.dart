import 'package:flutter/material.dart';
import 'package:interview/ItemList.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      initialRoute: 'listItem',
      routes: {
        'listItem': (context) => ListItem()
      },
    );
  }
}