class PhotoModels {
  List<PhotoModel> itemList = new List();

  PhotoModels.fromJsonList(List<dynamic> jsonList) {
    jsonList.forEach((json) {
      itemList.add(PhotoModel.fromJson(json));
    });
  }
}

class PhotoModel {
  String src;
  String title;
  String desc;
  bool favorite;

  PhotoModel({this.src, this.title, this.desc, this.favorite});

  PhotoModel.fromJson(Map<String, dynamic> json){
    src = json['src'];
    title = json['title'];
    desc = json['desc'];
    favorite = json['favorite'];
  }
}


class Muestras{
  List<Muestra> listaMuestras = new List();

  Muestras.fromJsonList(List<dynamic> jsonList){
    jsonList.forEach((muestra){
      listaMuestras.add(Muestra.fromJsonMap(muestra));
    });
  }
}


class Muestra{
  int id;
  int min;
  int max;
  int tamMuestra;

  Muestra({this.id, this.min, this.max, this.tamMuestra});

  Muestra.fromJsonMap(Map<String,dynamic> json){
    id = int.parse(json['id']);
    min = int.parse(json['min']);
    max = int.parse(json['max']);
    tamMuestra = int.parse(json['tamMuestra']);
  }

}