import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:interview/PhotoModel.dart';

class Provider{
  Future<PhotoModels> providerMethod() async {
    final resp = await rootBundle.loadString('assets/data.json');


    List itemList = json.decode(resp);
    final photoList = PhotoModels.fromJsonList(itemList);




    return photoList;
  }

}