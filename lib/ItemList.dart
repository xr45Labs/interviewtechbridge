import 'package:flutter/material.dart';
import 'package:interview/PhotoModel.dart';
import 'package:interview/provider.dart';


class ListItem extends StatelessWidget{

  final _provider = new Provider();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('List Item View'),
      ),
      body: Container(
        child: FutureBuilder(
          future: _provider.providerMethod(),
          builder: (BuildContext context, AsyncSnapshot<PhotoModels> snapshot){
            //print('listview____');
            if(snapshot.hasData){
              return ListView.builder(
                itemCount: snapshot.data.itemList.length,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    leading: CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(snapshot.data.itemList[index].src)
                    ),
                    title: Text(snapshot.data.itemList[index].title),
                    subtitle: Text(snapshot.data.itemList[index].desc),
                    trailing: (snapshot.data.itemList[index].favorite) ? Icon(Icons.star, color: Colors.blue,) : Icon(Icons.star_border, color: Colors.grey,),
                  );
                },
              );
            }
            return CircularProgressIndicator();
          },
        )
      ),
    );
  }

}

